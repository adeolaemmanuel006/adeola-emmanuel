import React from "react";
import Layout from "./containers/layout";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from "./containers/home";
import Skills from "./containers/skills";
import About from "./containers/about";
import Contact from "./containers/contact";

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/skills" element={<Skills />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
};

export default App;
