const Button: React.FC<Button> = ({ children, style, className, variant = 'filled' }) => {
  return (
    <button
      className={`${className} ${
        variant === "filled" ? "button-filled" : "button-outline"
      }`}
      style={style}
    >
      {children}
    </button>
  );
};

export default Button;
