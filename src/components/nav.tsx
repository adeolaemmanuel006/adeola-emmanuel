import Row from "./row";
import "../styles/nav.css";
import { Link } from "react-router-dom";
import Container from "./container";
import { RiBarChartHorizontalFill } from "react-icons/ri";
import React from "react";
import { motion, AnimatePresence } from "framer-motion";
// import { AiFillHome } from "react-icons/ai";
// import { MdOutlineContactPhone } from "react-icons/md";

const Nav: React.FC = () => {
  const [showSideBar, setShowSideBar] = React.useState("none");

  const meunAnimation = {
    initial: {
      x: "0",
      y: "0",
      opacity: 0,
    },
    animate: {
      x: "0%",
      y: "0%",
      opacity: 1,
      transition: {
        duration: 0.5,
        width: "60%",
      },
    },
    exit: {
      x: "50%",
    },
  };

  return (
    <Container
      style={{
        backgroundColor: "white",
        position: "sticky",
        top: 0,
        zIndex: 900,
      }}
    >
      <nav className="nav">
        <Row className="nav-wrap">
          <div>
            <Link to={"/"} className="link-a">
              Emmanuel
            </Link>
          </div>
          <Row className="links">
            <Link to={"/"} className="link-b">
              Home
            </Link>
            <a href={"/#services"} className="link-b">
              Services
            </a>
            <Link to={"/about"} className="link-b">
              About
            </Link>
            <Link to={"/contact"} className="link-b">
              Contact
            </Link>
          </Row>
          <div className="bar">
            <RiBarChartHorizontalFill
              onClick={() => {
                if (showSideBar === "none") {
                  setShowSideBar("block");
                } else {
                  setShowSideBar("none");
                }
              }}
              size={30}
              className="link-b"
              style={{ color: "black", paddingTop: 10 }}
            />
          </div>
        </Row>
      </nav>
      <AnimatePresence exitBeforeEnter>
        <motion.div
          className="side-bar"
          id="sideBar"
          variants={meunAnimation}
          initial="initial"
          animate="animate"
          exit="exit"
          style={{ display: showSideBar }}
        >
          <div style={{ paddingLeft: 30, paddingTop: 30 }}>
            <div style={{ paddingTop: 20 }}>
              {/* <AiFillHome size={30} color="#a8a8a8" /> */}
              <Link
                style={{
                  paddingTop: -10,
                  paddingLeft: 10,
                  fontSize: 20,
                  fontWeight: "bold",
                  display: "inline",
                }}
                to={"/"}
                className="link-b"
                onClick={() => setShowSideBar("none")}
              >
                Home
              </Link>
            </div>
            <div style={{ paddingTop: 30 }}>
              {/* <AiFillHome size={30} color="#a8a8a8" /> */}
              <a
                style={{
                  paddingTop: -10,
                  paddingLeft: 10,
                  fontSize: 20,
                  fontWeight: "bold",
                  display: "inline",
                }}
                href={"/#services"}
                className="link-b"
                onClick={() => setShowSideBar("none")}
              >
                Service
              </a>
            </div>
            <div style={{ paddingTop: 30 }}>
              {/* <AiFillHome size={30} color="#a8a8a8" /> */}
              <Link
                style={{
                  paddingTop: -10,
                  paddingLeft: 10,
                  fontSize: 20,
                  fontWeight: "bold",
                  display: "inline",
                }}
                to={"/about"}
                className="link-b"
                onClick={() => setShowSideBar("none")}
              >
                About
              </Link>
            </div>
            <div style={{ paddingTop: 30 }}>
              {/* <MdOutlineContactPhone size={30} /> */}
              <Link
                style={{
                  paddingTop: -10,
                  paddingLeft: 10,
                  fontSize: 20,
                  fontWeight: "bold",
                  display: "inline",
                }}
                to={"/contact"}
                className="link-b"
                onClick={() => setShowSideBar("none")}
              >
                Contact
              </Link>
            </div>
          </div>
        </motion.div>
      </AnimatePresence>
    </Container>
  );
};

export default Nav;
