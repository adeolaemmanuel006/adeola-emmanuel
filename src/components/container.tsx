import React, { Fragment } from "react";
import "../styles/container.css";
import Tilt from "react-parallax-tilt";

const Container: React.FC<Container> = ({
  className,
  children,
  animate,
  type,
  style,
  backgroundImage,
  backgroundWidth,
}) => {
  return (
    <Fragment>
      {animate === "parallax" ? (
        <Tilt
          className={`${className} ${type?.join(" ").toString()}`}
          style={style}
        >
          {children}
        </Tilt>
      ) : (
        <section
          className={`${className} container ${type?.join(" ").toString()} ${
            backgroundImage && "bg-image"
          }`}
          style={{ ...style }}
        >
          {backgroundImage && (
            <img
              src={backgroundImage}
              alt={`${backgroundImage ? "header-background" : ""}`}
              className={`${backgroundImage && "image"}`}
              style={{ width: backgroundWidth }}
            />
          )}
          {children}
        </section>
      )}
    </Fragment>
  );
};

export default Container;
