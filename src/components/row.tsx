import React from "react";

const Row: React.FC<Row> = ({
  children,
  style,
  className,
  type = "flex-flow",
  columnCount,
  justifyContent,
  key = "",
  id
}) => {
  return (
    <section
      className={`${className} ${
        type === "flex-flow" ? "row" : "row2"
      } ${justifyContent}`}
      style={style}
      key={key}
      id={id}
    >
      {React.Children.map(children, (child?: any) => {
        if (child) {
          return React.cloneElement(child, {
            className: `${child?.props?.className} row-col-count-${columnCount}`,
          });
        }
      })}
      {/* {children} */}
    </section>
  );
};

export default Row;
