import { Fragment } from "react";
import { ParallaxProvider } from "react-scroll-parallax";
import Nav from "../components/nav";

const Layout: React.FC<Layout> = ({ children }) => {
  return (
    <Fragment>
      <Nav />
      <ParallaxProvider>{children}</ParallaxProvider>
    </Fragment>
  );
};

export default Layout;
