import Button from "../components/button";
import Container from "../components/container";

const Contact: React.FC = () => {
  return (
    <div className="cont">
      <Container type={["card2"]} className="section">
        <div className="section-con">
          <h3 className="section-heading" style={{ paddingTop: 10 }}>
            Contact
          </h3>
          <form className="contact-con">
            <input className="input" placeholder="Name" />
            <input className="input" placeholder="Email Address" />
            <textarea className="text-area"></textarea>
            <Button className="contact-btn">Send</Button>
          </form>
        </div>
      </Container>
    </div>
  );
};

export default Contact;
