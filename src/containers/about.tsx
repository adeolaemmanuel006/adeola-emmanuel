import Container from "../components/container";
import { icons, image } from "../utils/assets";
import "../styles/about.css";
import Row from "../components/row";
import { about, education, experience, language } from "../utils/utils";
import { isMobile } from "react-device-detect";
import React from "react";

const About: React.FC<About> = () => {
  const cacheLength = isMobile ? 4 : 10;
  const cacheExp = isMobile ? 2 : 3;
  const cacheEdu = isMobile ? 3 : 5;
  const [langLength, setLangLength] = React.useState(cacheLength);
  const [expLength, setExpLength] = React.useState(cacheExp);
  const [eduLength, setEduLength] = React.useState(cacheEdu);
  const [lang, setLang] = React.useState("View all");
  const [exp, setExp] = React.useState("View all");
  const [edu, setEdu] = React.useState("View all");
  return (
    <div className="cont">
      <Container
        className="header"
        backgroundImage={image.backgroundCv}
        type={["card2"]}
      >
        <div className="inner">
          <Container
            backgroundImage={image.orisha}
            className="avatar"
            backgroundWidth="100%"
          ></Container>
          <h2 className="about-name">Emmanuel Adeola</h2>
          <p className="about-intro">Fullstack Engineer and Mobile Develper</p>
          <Row className="inner-social-con" justifyContent="jc-sb">
            <a href="https://github.com/Adeolaemmanuel">
              <img src={icons.github} alt="github" className="inner-social" />
            </a>
            <a href="https://gitlab.com/adeolaemmanuel006">
              <img src={icons.gitlab} alt="gitlab" className="inner-social" />
            </a>
          </Row>
        </div>
      </Container>

      <Container type={["card2"]} className="section">
        <Row className="section-con" justifyContent="jc-sb">
          <div className="about-details">
            <h3 className="section-heading">About me</h3>
            <p className="about-summary">
              Innovative and Passionate about products that make life easier for
              individuals at a micro-scale. Keen on knowledge— independent
              study, collaborative learning, and instilling knowledge to others.
              Team player.
            </p>
          </div>
          <div className="about-details-2">
            <h3 className="section-heading bio">Bio</h3>
            <div className="about-summary">
              {about.map(({ Icon, name, value }, ind) => {
                return (
                  <Row
                    style={{ marginTop: 9, paddingBottom: 20 }}
                    justifyContent="jc-sb"
                    key={ind}
                  >
                    <div style={{ width: "30%" }}>
                      <Icon /> {name}
                    </div>
                    <div style={{ width: "70%" }}>
                      <p>{value}</p>
                    </div>
                  </Row>
                );
              })}
            </div>
          </div>
        </Row>
      </Container>

      <Container type={["card2"]} className="section">
        <div className="section-con">
          <h3 className="section-heading">Professional Skills</h3>
          <Row columnCount="5" justifyContent="jc-sb" style={{ marginTop: 20 }}>
            {language.slice(0, langLength).map((data, ind) => {
              return (
                <Container
                  key={ind}
                  className="about-lang"
                  animate={"parallax"}
                  type={["card-hover", "plain-card"]}
                >
                  <img src={data.icon} className="lang-img" alt={data.name} />
                  <p className="about-lang-text">{data.name}</p>
                  <Row type="flex-direction" className="star" key={ind}>
                    {data.rate.map((data, indx) => {
                      return (
                        <img
                          src={icons.star}
                          alt={"about-star"}
                          className="about-star-img"
                          key={indx}
                        />
                      );
                    })}
                  </Row>
                </Container>
              );
            })}
          </Row>
          <p
            className="link-b"
            style={{
              textAlign: "center",
              marginTop: 20,
              cursor: "pointer",
              textDecoration: "underline",
              paddingBottom: 15,
            }}
            onClick={() => {
              if (langLength <= language.length) {
                let value = langLength + language.length;
                setLangLength(value);
                setLang("View less");
              } else {
                setLangLength(cacheLength);
                setLang("View all");
              }
            }}
          >
            {lang}
          </p>
        </div>
      </Container>

      <Container type={["card2"]} className="section">
        <div className="section-con">
          <h3 className="section-heading">Work Experience</h3>
          {experience.slice(0, expLength).map((data, ind) => {
            return (
              <Container key={ind} className="experience">
                <div className="exp-title">
                  <span style={{ fontSize: 17, fontWeight: "bold" }}>
                    {data.name}
                  </span>
                  <span style={{ paddingLeft: 5 }}>at</span>
                  {data.link ? (
                    <a href={data.link}>
                      <span style={{ paddingLeft: 5, color: "#8d8d8d" }}>
                        {data.company}
                      </span>
                    </a>
                  ) : (
                    <span style={{ paddingLeft: 5 }}>{data.company}</span>
                  )}
                </div>
                <div className="exp-details">
                  <p style={{ fontSize: 11 }}>
                    {data.dateStart} - {data.dateEnd}
                  </p>
                  <li style={{ paddingTop: 2 }}>
                    {data.description?.map((exp, indx) => {
                      return (
                        <ul style={{ paddingTop: 12 }} key={indx}>
                          {exp}
                        </ul>
                      );
                    })}
                  </li>
                </div>
              </Container>
            );
          })}
          <p
            className="link-b"
            style={{
              textAlign: "center",
              marginTop: 20,
              paddingBottom: 15,
              cursor: "pointer",
              textDecoration: "underline",
            }}
            onClick={() => {
              if (expLength <= experience.length) {
                let value = expLength + experience.length;
                setExpLength(value);
                setExp("View less");
              } else {
                setExpLength(cacheExp);
                setExp("View all");
              }
            }}
          >
            {exp}
          </p>
        </div>
      </Container>

      <Container type={["card2"]} className="section">
        <div className="section-con">
          <h3 className="section-heading">Education</h3>
          {education.slice(0, expLength).map((data, ind) => {
            return (
              <Container key={ind} className="experience">
                <div className="edu-title">
                  <span style={{ fontSize: 17, fontWeight: "bold" }}>
                    {data.school}
                  </span>
                </div>
                <div className="exp-details">
                  <p style={{ fontSize: 11 }}>
                    {data.dateStart} - {data.dateEnd}
                  </p>

                  {data.link ? (
                    <a href={data.link}>
                      <p className="link-b" style={{ marginTop: 10 }}>
                        {data.course}
                      </p>
                    </a>
                  ) : (
                    <p style={{ marginTop: 10 }}>{data.course}</p>
                  )}
                </div>
              </Container>
            );
          })}
          <p
            className="link-b"
            style={{
              textAlign: "center",
              marginTop: 20,
              cursor: "pointer",
              textDecoration: "underline",
              paddingBottom: 15,
            }}
            onClick={() => {
              if (eduLength <= education.length) {
                let value = eduLength + education.length;
                setEduLength(value);
                setEdu("View less");
              } else {
                setEduLength(cacheEdu);
                setEdu("View all");
              }
            }}
          >
            {edu}
          </p>
        </div>
      </Container>
    </div>
  );
};

export default About;
