import { Fragment } from "react";
import { isMobile } from "react-device-detect";
import { Link } from "react-router-dom";
import { Parallax } from "react-scroll-parallax";
import { icons } from "../utils/assets";
import Button from "../components/button";
import Container from "../components/container";
import Row from "../components/row";
import "../styles/home.css";
import { language } from "../utils/utils";

const Home: React.FC<Home> = ({ children }) => {
  const langLength = isMobile ? 4 : 5;
  return (
    <Fragment>
      <Container>
        <div className="home-wrap">
          <Row
            className="section-1"
            justifyContent="jc-sb"
            type="flex-direction"
          >
            <div className="intro-text">
              <h1 className="name">
                Hi ! <br /> I'm Emmanuel Adeola
              </h1>
              <p className="summary">
                I'm Innovative and Passionate about products that make life
                easier for individuals at a micro-scale. Keen on knowledge—
                independent study, collaborative learning, and instilling
                knowledge to others. Team player.
              </p>
              <Row justifyContent="jc-sb">
                <Button className="button" variant="outline">
                  <Link
                    to={"/contact"}
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    Hire me
                  </Link>
                </Button>
                <Button className="button">
                  <Link
                    to={"/about"}
                    style={{ textDecoration: "none", color: "black" }}
                  >
                    View CV
                  </Link>
                </Button>
              </Row>
            </div>
            {!isMobile && (
              <Parallax
                speed={-10}
                translateX={["-250px", "180px"]}
                easing="easeInOut"
              >
                <img src={icons.developer} className="dev" alt="developer" />
              </Parallax>
            )}
          </Row>
        </div>
      </Container>
      <Container style={{ backgroundColor: "white" }}>
        <div className="home-wrap">
          <Row justifyContent="jc-sb" style={{ marginTop: 20 }}>
            <p className="skills">Skills</p>
            <Link
              to={"/skills"}
              className="view-all"
              style={{ textDecoration: "underline" }}
            >
              View All
            </Link>
          </Row>
          {/* programming langauge */}
          <Row columnCount="5" justifyContent="jc-sb" style={{ marginTop: 20 }}>
            {language.slice(0, langLength).map((data, ind) => {
              return (
                <Container
                  key={ind}
                  className="lang"
                  animate={"parallax"}
                  type={["card-hover", "plain-card"]}
                >
                  <img src={data.icon} className="lang-img" alt={data.name} />
                  <p className="lang-text">{data.name}</p>
                  <Row type="flex-direction" className="star">
                    {data.rate.map((data, indx) => {
                      return (
                        <img
                          src={icons.star}
                          alt={"star"}
                          className="star-img"
                          key={indx}
                        />
                      );
                    })}
                  </Row>
                </Container>
              );
            })}
          </Row>
          {/* All works */}
          <Row className="all-works">
            <div style={{ width: "50%" }}>
              {!isMobile && (
                <Parallax
                  speed={-10}
                  translateX={["-250px", "0px"]}
                  easing="easeInOut"
                >
                  <img src={icons.developer2} className="dev" alt="developer" />
                </Parallax>
              )}
            </div>
            <div className="works" id="services">
              <Container
                animate={"parallax"}
                className="work-list"
                type={["plain-card", "card-hover"]}
              >
                <Row>
                  <div className="work-img">
                    <img
                      src={icons.frontend}
                      style={{ width: "40%", marginTop: 15 }}
                      alt="front-end"
                    />
                  </div>
                  <p className="work-text">Front End Engineer</p>
                </Row>
              </Container>
              <Container
                animate={"parallax"}
                className="work-list"
                type={["plain-card", "card-hover"]}
              >
                <Row>
                  <div className="work-img">
                    <img
                      src={icons.backend}
                      style={{ width: "40%", marginTop: 15 }}
                      alt="front-end"
                    />
                  </div>
                  <p className="work-text"> Back End Engineer</p>
                </Row>
              </Container>
              <Container
                animate={"parallax"}
                className="work-list"
                type={["plain-card", "card-hover"]}
              >
                <Row>
                  <div className="work-img">
                    <img
                      src={icons.mobile}
                      style={{ width: "40%", marginTop: 15 }}
                      alt="front-end"
                    />
                  </div>
                  <p className="work-text"> Mobile Developer</p>
                </Row>
              </Container>
            </div>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
};

export default Home;
