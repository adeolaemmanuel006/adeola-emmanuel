import { icons } from "../utils/assets";
import Container from "../components/container";
import Row from "../components/row";
import { language } from "../utils/utils";

const Skills: React.FC = () => {
  return (
    <Container style={{ backgroundColor: "white" }}>
      <div className="home-wrap">
        {/* programming langauge */}
        <Row
          columnCount="5"
          style={{ justifyContent: "space-between", paddingTop: 50 }}
        >
          {language.map((data, ind) => {
            return (
              <Container
                key={ind}
                className="lang"
                animate={"parallax"}
                type={["card-hover", "plain-card"]}
              >
                <img src={data.icon} className="lang-img" alt={data.name} />
                <p className="lang-text">{data.name}</p>
                <Row type="flex-direction" className="star">
                  {data.rate.map((data, indx) => {
                    return (
                      <img
                        src={icons.star}
                        alt={"star"}
                        className="star-img"
                        key={indx}
                      />
                    );
                  })}
                </Row>
              </Container>
            );
          })}
        </Row>
      </div>
    </Container>
  );
};

export default Skills;
