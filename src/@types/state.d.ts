interface Experience {
  name: string;
  company: string;
  dateStart: string;
  dateEnd?: string | "present";
  description?: string[];
  link?: string
}
interface Education {
  description?: string;
  school: string;
  course: string;
  dateStart: string;
  dateEnd?: string | "present";
  link?: string;
}
