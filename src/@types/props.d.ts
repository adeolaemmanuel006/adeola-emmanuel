interface General
  extends React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLDivElement>,
    HTMLDivElement
  > {
  children?: Children;
}
type Card = "card" | "card-hover" | "plain-card" | 'card2';
interface Row extends General {
  type?: "flex-flow" | "flex-direction";
  columnCount?: "1" | "2" | "3" | "4" | "5" | "6";
  justifyContent?: "jc-sb" | "js-sa" | "js-se";
}
interface Button extends General {
  variant?: "outline" | "filled";
  link?: string
}

interface Container extends General {
  animate?: "parallax" | 'bounce' | null;
  type?: Card[];
  backgroundImage?: string
  backgroundWidth?: string
  overlay?: number
  overlayColor?: string
}
interface Layout extends General {}
interface Home extends General {}
interface About extends General {}
interface ProgressBar extends General {
  color?: string
}
