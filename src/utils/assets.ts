import js from "../assets/icons/js.png";
import ts from "../assets/icons/ts.png";
import nodejs from "../assets/icons/nodejs.png";
import react from "../assets/icons/react.png";
import angular from "../assets/icons/angular.png";
import expressjs from "../assets/icons/expressjs.png";
import star from "../assets/icons/star.png";
import developer from "../assets/icons/developer.svg";
import nestjs from "../assets/icons/nestjs.svg";
import developer2 from "../assets/icons/developer2.png";
import frontend from "../assets/icons/frontend.png";
import backend from "../assets/icons/backend.png";
import mobile from "../assets/icons/mobile.png";
import postgres from "../assets/icons/postgres.png";
import firebase from "../assets/icons/firebase.png";
import gcp from "../assets/icons/gcp.png";
import github from "../assets/icons/github.png";
import gitlab from "../assets/icons/gitlab.png";
import orisha from "../assets/images/orisha.png";
import sql from "../assets/icons/sql.png";
import backgroundCv from "../assets/images/header-background.jpg";

export const icons = {
  js,
  ts,
  nodejs,
  react,
  angular,
  expressjs,
  star,
  developer,
  frontend,
  backend,
  mobile,
  developer2,
  postgres,
  firebase,
  gcp,
  nestjs,
  github,
  gitlab,
  sql,
};

export const image = {
  backgroundCv,
  orisha,
};
