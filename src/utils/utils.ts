import { AiOutlinePhone } from "react-icons/ai";
import { HiOutlineMail } from "react-icons/hi";
import { MdLocationPin } from "react-icons/md";
import { icons } from "./assets";

export const language = [
  { name: "JavaScript", icon: icons.js, rate: [1, 2, 3, 4, 5] },
  { name: "TypeScript", icon: icons.ts, rate: [1, 2, 3, 4, 5] },
  { name: "NodeJs", icon: icons.nodejs, rate: [1, 2, 3, 4] },
  { name: "ExpressJs", icon: icons.expressjs, rate: [1, 2, 3, 4, 5] },
  { name: "React", icon: icons.react, rate: [1, 2, 3, 4] },
  { name: "React Native", icon: icons.react, rate: [1, 2, 3, 4] },
  { name: "Angular", icon: icons.angular, rate: [1, 2, 3, 4] },
  { name: "Postgres", icon: icons.postgres, rate: [1, 2, 3, 4] },
  { name: "Firebase", icon: icons.firebase, rate: [1, 2, 3, 4] },
  { name: "GCP", icon: icons.gcp, rate: [1, 2, 3, 4] },
  { name: "NestJs", icon: icons.nestjs, rate: [1, 2, 3, 4] },
  { name: "SQL", icon: icons.sql, rate: [1, 2, 3, 4] },
];

export const about = [
  { name: "Email", value: "adeolaemmanuel006@gmail.com", Icon: HiOutlineMail },
  { name: "Phone", value: "09030834160", Icon: AiOutlinePhone },
  {
    name: "Address",
    value: "4 Ajuin, off awofodu street, somolu Lagos.",
    Icon: MdLocationPin,
  },
];

export const experience: Experience[] = [
  {
    name: "Full-Stack Engineer",
    company: "SALAD AFRICA",
    dateStart: "June, 2022",
    dateEnd: "Present",
    description: [
      "Bootstrapped backend server used by front-end built with expressJs.",
      "Maintains express Js back end and makes new implements when needed.",
      "Built the company's main website.",
      "Upgraded system architecture for third party API integration with other services like sudo, verifyme, paystack.",
      "Monitors PostgresQL database schemas and migration.",
      "Software quality assurance.",
    ],
    link: "https://www.saladafrica.com",
  },

  {
    name: "Full-Stack Engineer",
    company: "SHOWLOVE",
    dateStart: "July, 2021",
    dateEnd: "June, 2022",
    description: [
      "Maintains NestJs back end and implements new modules(PostgreSQL Entity) with graphql.",
      "Implemented a new feature called wishlist where users can add and remove items from their wishlist.",
      "Integrated system(NestJs backend) with Hasura cloud for easy query with graphql.",
      "Integrated the system(NestJs backend) with Bull Redis to queue orders and notification being sent to users.",
      "Monitors PostgresQL database schemas and migration.",
      "Track project deliverables using appropriate tools.",
      "Software quality assurance.",
    ],
    link: "https://showlove.io/",
  },
  {
    name: "Mobile Developer",
    company: "HashBang",
    dateStart: "Jan, 2021",
    dateEnd: "April, 2021",
    description: [
      "Developed a React native mobile app called Convey.",
      "Integrated google maps that tracks both merchant and user location.",
      "Facilitate the definition of project scope, goals and deliverables.",
      "Define project tasks and resource requirements.",
      "Plan and schedule project timelines.",
      "Track project deliverables using appropriate tools.",
      "Present reports defining project progress, problems.",
    ],
  },
  {
    name: "Front-End Engineer",
    company: "KIGENNI",
    dateStart: "Jan, 2019",
    dateEnd: "Dec, 2021",
    description: [
      "Monitored and updated customer database.",
      "Assisted in analyzing and proffered solution to software application Problems",
      "Upgraded writing and review tools.",
      "Maintained and upgraded front-end and back-end of the website platform.",
      "Developed RESTful/Realtime API Automated Resume Builder application using NodeJS + Express.",
      "Track project deliverables using appropriate tools.",
      "Facilitate the definition of project scope, goals and deliverables",
    ],
    link: "https://www.kigenni.com/",
  },
  {
    name: "Front-End Developer",
    company: "EXPAT SYSTEM SOLUTION",
    dateStart: "Dec, 2018",
    dateEnd: "Oct, 2019",
    description: [
      "Assisted in analyzing and proffered solution to software application Problems",
      "Worked and Improved on previous software application",
      "Monitored and updated customer database",
      "Presented designs to clients",
      "Engaged clients and recorded briefs and requests",
      "Designed websites and software applications(Reactjs) within team of 6",
    ],
  },
];

export const education: Education[] = [
  {
    description: "Udemy",
    course: "React - The Complete Guide (incl Hooks, React Router, Redux)",
    dateStart: "June, 2021",
    school: "Udemy",
    link: "https://www.udemy.com/certificate/UC-1b041fe9-b644-4b44-b659-730544c8d324/",
  },
  {
    description: "Bowen University",
    course: "B.Sc. Physics and Solar Energy",
    dateStart: "July, 2018",
    school: "Bowen University, Iwo",
  },
  {
    description: "Jextoban Secondary School",
    course: "Senior Secondary School Certificate",
    dateStart: "July, 2013",
    school: "Jextoban Secondary School",
  },
];
